# PPM Environment #

## Requirements ##

* Apache Ant 1.8 or later
* A Java development kit for Java 7 or later (javac is needed)

## Setup ##
In order to set up your clone of the PPM Environment (PPME) project (including MPS) run the following set of commands:

```
#!shell

git clone git@bitbucket.org:ppme/ppme.git
cd ppme
git submodule update --init --recursive
ant
```

This will fetch a copy of JetBrains Meta Programming System (MPS) and run the build scripts for all submodules, i.e., the main project and its plugins and extensions.

Once the download is finished and everything is set up you can start MPS and open the project located at `./ppme-lang`. Right click on the root node (PPME-Lang) and select "Rebuild project" from the drop-down menu. This will generate all contained languages so that they are ready for use.

For a more detailed tutorial on how to set up PPME and PPM go to the [PPME Wiki](https://bitbucket.org/ppme/ppme/wiki/Home).

## Update ##

To update the whole project just run the following commands. This will update the this repo and all submodules (and
checkout their default branches). Be sure to checkout your working branch in ppme-lang and update it as well.

```
#!shell

git pull origin master
git submodule update --recursive
```

## License ##
This project, _PPME_ is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

- - - 
[PPME@bitbucket](https://bitbucket.org/ppme/ppme) | [PPME-Lang@bitbucket](https://bitbucket.org/ppme/ppme-lang)